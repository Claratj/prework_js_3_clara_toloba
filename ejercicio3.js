
function comprobacionNumDNI(numDNI, letraDNI) {
    
    var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 
                            'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
    numDNI = window.prompt("Introduzca número DNI");                       
    letraDNI = window.prompt("Introduzca letra DNI");    

    if (numDNI < 0 || numDNI > 99999999) {
      document.write("Número proporcionado no válido");
    } else {
     var resto = numDNI % 23;
      if (letras[resto] == letraDNI) {
        document.write("DNI válido");
      } else {
        document.write("DNI inválido");
      }
    }
}

// comprobacionNumDNI(51985743, "V");
comprobacionNumDNI(51985743, "Q");
